import { Component, OnInit,ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({ selector: 'app-root', templateUrl: 'app.component.html'})
export class AppComponent implements OnInit {
	registerForm: FormGroup;
	submitted = false;
	public Editor = ClassicEditor;
	public couponAvaliability = [
		{value:true,viewValue:"Unlimited"},
		{value:false,viewValue:"Not Unlimited"}
	];
	public couponsJsonData;
	@ViewChild("variableRef", { static: false }) variableRef: ModalDirective;
	
	constructor(private formBuilder: FormBuilder) { }

	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			coupon_type: ['', Validators.required],
			coupon_code: ['', Validators.required],
			valid_from: ['', [Validators.required,Validators.pattern(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/)]],
			// validates date format yyyy-mm-dd
			valid_to: ['', [Validators.required, Validators.pattern(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/)]],
			is_unlimited: [, Validators.required],
			coupon_count:[],
			coupon_status:[],
			tnc:['Type Something.'],
			rules: this.formBuilder.array([this._addRules()])
		});
	}
	
	private _addRules(): FormGroup {
		return this.formBuilder.group({
			min_amount:['',Validators.required],
			max_amount:['',Validators.required],
			discount_type:['',Validators.required],
			discount:[,Validators.required],
			max_discount:[]
		});
	  }

	// convenience getter for easy access to form fields
	get f() { 
		return this.registerForm.controls; 
	}

	get rules():FormArray{
		return this.registerForm.get('rules') as FormArray;
	}

	addRule() {
		this.rules.push(this._addRules());
	}

	ruleFormGruopControls(index:number) {
		return ((this.registerForm.get('rules') as FormArray).controls[index] as FormGroup).controls;
	}

	removeRule(index:number) {
		this.rules.removeAt(index);
	}

	onSubmit() {
		// display form values on success
		this.couponsJsonData = this.registerForm.value;
		let {is_unlimited} = this.couponsJsonData;
		if(is_unlimited=="false"){
			delete this.couponsJsonData.coupon_count;
		}
		this.openVariablesFormDialog();


		this.submitted = true;
		// stop here if form is invalid
		if (this.registerForm.invalid) {
			return;
		}
	}

	onReset() {
		this.submitted = false;
		this.registerForm.reset();
	}
	
	openVariablesFormDialog() {
		this.variableRef.show();
	}

	hideVariablesFormDialog() {
		this.variableRef.hide();
	}

}
